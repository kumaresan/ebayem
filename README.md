# eBay EM  (*em*bedded eBay)

eBay embedded is a android library for building shopping apps powered by eBay Public Apis.

## Commorce Componenets!

  - Search
  - View Item
  - Guest checkout (Work in Progress)
  - Login with eBay (Work in Progress)
  - Member checkout (Work in Progress)


## Getting started in 5 easy steps

### Step 1) Registration
- Register at [developer.ebay.com](https://developer.ebay.com)
- Get the client id and client secret from eBay developer portal.

### Step 2) Add eBay Em library to your app
Add ebayem android library as a dependency. **transitive** should be **true** to download sub dependencies as well.

```sh
dependencies {
    ...
    compile('com.bitbucket.kumaresan:ebayem:0.0.8@aar'){
        transitive = true
    }
    ....
}
```

### Step 3) Hacks to avoid dependency conflict

Enable **multiDex** to avoid dependency conflicts during compilation.
```sh
defaultConfig {
    ...
    multiDexEnabled true
    ....
}
```

Exclude unnecessary files from packaging to avoid errors during production release.
```sh
android {
    ...
    packagingOptions {
        exclude 'META-INF/DEPENDENCIES.txt'
        exclude 'META-INF/LICENSE.txt'
        exclude 'META-INF/NOTICE.txt'
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/LICENSE'
        exclude 'META-INF/DEPENDENCIES'
        exclude 'META-INF/notice.txt'
        exclude 'META-INF/license.txt'
        exclude 'META-INF/dependencies.txt'
        exclude 'META-INF/LGPL2.1'
    }
    ....
}
```

### Step 4 Initilize eBay EM
Initilise the eBayEM lib by setting **client id** and **client secret** from eBay developer portal.
```sh
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ...
        ...
        String clientId = "<<Your eBay Client Id>>";            // Client id from eBay Developer Portal
        String clientSecret = "<<Your eBay Client Secret>>";    // Client secret from eBay Developer Portal
        Boolean isProduction = true;                            // Sandbox is false

        EBayAppConfig eBayAppConfig = new EBayAppConfig(clientId,clientSecret,isProduction);
        EBayEM.getInstance().initialize(getApplicationContext(),eBayAppConfig);
        ...
        ...
    }
```
#### Notes :
- Best is to initialize this during onCreate of you main activity.
- Storing client id and secret in plain text can be unsecure. [Learn](http://stackoverflow.com/questions/28609526/store-client-secret-securely) about securing them.


### Step 5 Invoke embedded views EBayEM
Invoking the search view for your for "iPhone"
```sh
    @Override
    public void onClick(View view) {
        String searchQuery = "iPhone";
        EBayEM.getInstance().search(view.getContext(),searchQuery);
    }
```

Invoking the item view for an item.
```sh
    @Override
    public void onClick(View view) {
        String itemId = "v1|<itemid>|0";
        EBayEM.getInstance().item(view.getContext(),itemId);
    }
```
