package com.ebay.api.ebayem;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void testEbayAppConfig() throws Exception {
        String clientId = "clientId";
        String clientSecret = "clientSecret";
        Boolean isProduction = true;
        EBayAppConfig appConfig = new EBayAppConfig(clientId,clientSecret,isProduction);
        assert clientId == appConfig.getClientId();
        assert clientSecret == appConfig.getClientSecret();
        assert appConfig.getIsProduction();
    }
}