package com.ebay.api.ebayem;

/**
 * Constants used by the eBay Activity
 * Created by @Kumaresan on 3/8/2017.
 */

public interface Constants {

    public static final String ARG_ACTION = "arg_action";
    public static final String ARG_SEARCH_QUERY = "arg_search_query";
    public static final String ARG_ITEM_ID = "arg_item_id";

    //TODO convert this to enum
    public static final Integer ACTION_SEARCH = 1;
    public static final Integer ACTION_VIEW_ITEM = 2;
}
