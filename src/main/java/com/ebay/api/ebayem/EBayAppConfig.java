package com.ebay.api.ebayem;

import android.support.annotation.NonNull;
import android.util.Base64;

/**
 * Represents the Authorization of the eBay Application
 *
 * Created by @Kumaresan on 3/6/2017.
 * @See https://developer.ebay.com/devzone/rest/ebay-rest/content/oauth-gen-app-token.html
 */

public class EBayAppConfig {
    private final String clientId;
    private final String clientSecret;
    private final Boolean isProduction;

    public EBayAppConfig(@NonNull String clientId, @NonNull String clientSecret, @NonNull Boolean isProduction) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.isProduction = isProduction;
    }

    public String getClientId(){
        return this.clientId;
    }

    public String getClientSecret(){
        return this.clientSecret;
    }

    public Boolean getIsProduction() {
        return isProduction;
    }


}
