package com.ebay.api.ebayem;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.ebay.api.client.buy.BrowseApi;
import com.ebay.api.client.buy.OrderApi;

/**
 * Main Api for eBay embedded library
 * <p>
 * Created by @Kumaresan on 3/6/2017.
 */

public class EBayEM {

    private static volatile EBayEM singleton;

    static final String TAG = "EBayEM";

    final AuthorizationHelper authorizationHelper;
    final SearchHelper searchHelper;
    final ItemHelper itemHelper;

    final BrowseApi browseApi;
    final OrderApi orderApi;

    final Boolean loggingEnabled;

    EBayEM(BrowseApi browseApi,
           OrderApi orderApi,
           AuthorizationHelper authorizationHelper,
           SearchHelper searchHelper,
           ItemHelper itemHelper,
           Boolean loggingEnabled) {

        super();

        this.browseApi = browseApi;
        this.orderApi = orderApi;

        this.authorizationHelper = authorizationHelper;
        this.searchHelper = searchHelper;
        this.itemHelper = itemHelper;

        this.loggingEnabled = loggingEnabled;
    }

    public static EBayEM getInstance() {
        if (singleton == null) {
            synchronized (EBayEM.class) {
                if (singleton == null) {
                    singleton = new Builder().build();
                }
            }
        }
        return singleton;
    }

    public void initialize(Context context, EBayAppConfig eBayAppConfig) {
        this.authorizationHelper.seteBayAppConfig(eBayAppConfig);
    }

    public void search(Context context, String searchQuery){
        Intent intent = this.searchHelper.getSearchIntent(context, searchQuery);
        context.startActivity(intent);
    }

    public void item(Context context, String itemId){
        Intent intent = this.itemHelper.getItemIntent(context, itemId);
        context.startActivity(intent);
    }

    public AuthorizationHelper getAuthorizationHelper() {
        return authorizationHelper;
    }

    public SearchHelper getSearchHelper() {
        return searchHelper;
    }

    public ItemHelper getItemHelper() {
        return itemHelper;
    }

    public BrowseApi getBrowseApi() {
        browseApi.getInvoker().setApiKey(getAuthorizationHelper().getAppAuthorization());
        return browseApi;
    }

    public OrderApi getOrderApi() {
        return orderApi;
    }


    public static void setSingletonInstance(@NonNull EBayEM ebayEM) {
        if (ebayEM == null) {
            throw new IllegalArgumentException("ebayEM must not be null.");
        }
        synchronized (EBayEM.class) {
            if (singleton != null) {
                throw new IllegalStateException("Singleton instance already exists.");
            }
            singleton = ebayEM;
        }
    }


    public static class Builder {

        private BrowseApi browseApi;
        private OrderApi orderApi;

        private AuthorizationHelper authorizationHelper;
        private SearchHelper searchHelper;
        private ItemHelper itemHelper;

        private Boolean loggingEnabled;

        /**
         * Register a {@link BrowseApi}.
         */
        public Builder browseApi(@NonNull BrowseApi browseApi) {
            this.browseApi = browseApi;
            return this;
        }

        /**
         * Register a {@link OrderApi}.
         */
        public Builder orderApi(@NonNull OrderApi orderApi) {
            this.orderApi = orderApi;
            return this;
        }


        /**
         * Register a {@link AuthorizationHelper}.
         */
        public Builder authorizationHelper(@NonNull AuthorizationHelper authorizationHelper) {
            this.authorizationHelper = authorizationHelper;
            return this;
        }

        /**
         * Register a {@link SearchHelper}.
         */
        public Builder searchHelper(@NonNull SearchHelper searchHelper) {
            this.searchHelper = searchHelper;
            return this;
        }

        /**
         * Register a {@link ItemHelper}.
         */
        public Builder itemHelper(@NonNull ItemHelper itemHelper) {
            this.itemHelper = itemHelper;
            return this;
        }


        /**
         * Toggle whether debug logging is enabled.
         * <p>
         * <b>WARNING:</b> Enabling this will result in excessive object allocation. This should be only
         * be used for debugging purposes. Do NOT pass {@code BuildConfig.DEBUG}.
         */
        public Builder loggingEnabled(Boolean enabled) {
            this.loggingEnabled = enabled;
            return this;
        }


        /**
         * Create the {@Link EBayEM} instance
         */
        public EBayEM build() {
            if (browseApi == null) {
                browseApi = new BrowseApi();
            }
            if (orderApi == null) {
                orderApi = new OrderApi();
            }

            if (authorizationHelper == null) {
                authorizationHelper = new AuthorizationHelper();
            }

            if (searchHelper == null) {
                searchHelper = new SearchHelper();
            }

            if (itemHelper == null) {
                itemHelper = new ItemHelper();
            }

            return new EBayEM(browseApi, orderApi, authorizationHelper, searchHelper,itemHelper,loggingEnabled);
        }
    }

}

