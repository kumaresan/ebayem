package com.ebay.api.ebayem;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import com.ebay.api.client.buy.model.Image;

/**
 * A simple class to for showing list of images.
 * Created by @Kumaresan on 2/27/2017.
 */

public class ImageSlider extends PagerAdapter {

    Context context;
    List<Image> imageList;

    public ImageSlider(Context context, List<Image> imageList ){
        this.context = context;
        this.imageList = imageList;

    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View viewItem = inflater.inflate(R.layout.fragment_image_slider, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.imageView);
        Picasso.with(context).load(imageList.get(position).getImageUrl()).into(imageView);
        ((ViewPager)container).addView(viewItem);
        return viewItem;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View)object);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }

}