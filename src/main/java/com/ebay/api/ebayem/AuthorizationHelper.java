package com.ebay.api.ebayem;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.JsonReader;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static com.android.volley.VolleyLog.TAG;

/**
 * TODO this class needs work.
 * Created by @Kumaresan on 3/6/2017.
 */

public class AuthorizationHelper {
    private static String TAG = "FetchAuthTokenHack";
    volatile EBayAppConfig eBayAppConfig;
    volatile String appAuthorizationToken;
    volatile Long appAuthorizationTokenExpiresAt;

    synchronized public void seteBayAppConfig(EBayAppConfig eBayAppConfig){
        this.eBayAppConfig = eBayAppConfig;
        fetchToken();
    }

    public String getAppAuthorization(){
        if(this.eBayAppConfig == null){
            throw new RuntimeException("eBay app config has not been set");
        }

        if(this.appAuthorizationToken == null){
            fetchToken();
            throw new RuntimeException("Authorization has not been generated yet. Try after sometime!");
        }

//        if(this.appAuthorizationTokenExpiresAt < System.currentTimeMillis()){
//            this.appAuthorizationToken = null;
//            this.appAuthorizationTokenExpiresAt = null;
//
//            fetchToken();
//            throw new RuntimeException("The Authorization token has expired. Try after sometime!");
//        }

        // refresh the auth token 1 hr before expiry
        if(this.appAuthorizationTokenExpiresAt < System.currentTimeMillis() + 300){
            // Refresh the token
            fetchToken();
        }

        return "Bearer " + this.appAuthorizationToken;
    }

    private void fetchToken(){
        Log.d(TAG,"fetchToken " + eBayAppConfig.toString() );
        Log.d(TAG,"fetchToken " + appAuthorizationToken );
        Log.d(TAG,"fetchToken " + appAuthorizationTokenExpiresAt);

        new FetchAuthTokenHack(this.eBayAppConfig, this).execute();
    }

    public String getBasicClientAuth() {
        String temp = eBayAppConfig.getClientId() + ":" + eBayAppConfig.getClientSecret();
        String basicClientAuth = "Basic " + new String(Base64.encode(temp.getBytes(),Base64.NO_WRAP));
        return basicClientAuth;
    }

    synchronized private void setToken(String appAuthorizationToken, Long appAuthorizationTokenExpiresAt){
        Log.d(TAG,"setToken " + appAuthorizationToken );
        Log.d(TAG,"setToken " + appAuthorizationTokenExpiresAt);
        this.appAuthorizationToken = appAuthorizationToken;
        this.appAuthorizationTokenExpiresAt = appAuthorizationTokenExpiresAt;
    }

    // TODO this is a hack, use swagger client instead.
    static class FetchAuthTokenHack extends AsyncTask<Void, Integer, Long> {
        private static String TAG = "FetchAuthTokenHack";

        final EBayAppConfig eBayAppConfig;
        final AuthorizationHelper authorizationHelper;

        public FetchAuthTokenHack(EBayAppConfig eBayAppConfig, AuthorizationHelper authorizationHelper) {
            this.eBayAppConfig = eBayAppConfig;
            this.authorizationHelper = authorizationHelper;
        }

        @Override
        protected Long doInBackground(Void... params) {
            Log.d(TAG, "doInBackground ");
            try {
                Log.d(TAG, "doInBackground Call API");

                getAuthTokenHACK();

                Log.d(TAG, "doInBackground Called API");
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "doInBackground Error! " + e.getMessage());

                return 1L;
            }
            Log.d(TAG, "doInBackground Returning 0");
            return 0L;
        }

        // TODO swagger based client for getAuthToken
        // All http post call via the android swagger client are failing so calling the api directly
        // Will remove this once the base issue is fixed.
        protected void getAuthTokenHACK() throws Exception {
            URL url = new URL("https://api.ebay.com/identity/v1/oauth2/token");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            String API_KEY = authorizationHelper.getBasicClientAuth();
            Log.d(TAG, "API_KEY : " + API_KEY);

            int timeoutMs = 30000;
            connection.setConnectTimeout(timeoutMs);
            connection.setReadTimeout(timeoutMs);
            connection.setUseCaches(false);
            connection.setDoInput(true);

            connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.addRequestProperty("Authorization", API_KEY);

            connection.setRequestMethod("POST");

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("grant_type", "client_credentials"));
//        params.add(new BasicNameValuePair("redirect_uri", this.eBayAppConfig.getRuName()));
            params.add(new BasicNameValuePair("scope", "https://api.ebay.com/oauth/api_scope"));

            byte[] body = getQuery(params).getBytes();

            connection.setDoOutput(true);

            DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            out.write(body);
            out.close();

            int responseCode = connection.getResponseCode();
            String responseMessage = connection.getResponseMessage();

            Log.d(TAG, responseMessage + " " + responseCode);

            BasicHttpEntity entity = new BasicHttpEntity();
            InputStream inputStream;
            try {
                inputStream = connection.getInputStream();
            } catch (IOException ioe) {
                inputStream = connection.getErrorStream();
            }
            entity.setContent(inputStream);
            entity.setContentLength(connection.getContentLength());
            entity.setContentEncoding(connection.getContentEncoding());
            entity.setContentType(connection.getContentType());

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            entity.writeTo(outputStream);

            String responseString = new String(outputStream.toByteArray());
            Log.d(TAG, responseString);

            Gson gson = new Gson();
            EBayAuthorization eBayAuthorization = gson.fromJson(responseString, EBayAuthorization.class);

            Log.d(TAG, eBayAuthorization.toString());

            if (responseCode == 200) {
                this.authorizationHelper.setToken(eBayAuthorization.access_token, System.currentTimeMillis() + Long.parseLong(eBayAuthorization.expires_in));
            } else {
                throw new Exception("Response Code is " + responseCode);
            }
        }


        private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
        {
            StringBuilder result = new StringBuilder();
            boolean first = true;

            for (NameValuePair pair : params)
            {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
            }

            return result.toString();
        }

        class EBayAuthorization {
            String access_token;
            String token_type;
            String expires_in;
            String refresh_token;

            @Override
            public String toString() {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("access_token:" + access_token + "\n");
                stringBuffer.append("token_type:" + token_type + "\n");
                stringBuffer.append("expires_in:" + expires_in + "\n");
                stringBuffer.append("refresh_token:" + refresh_token + "\n");
                return stringBuffer.toString();
            }
        }
    }



}
