package com.ebay.api.ebayem;

import android.app.Fragment;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.List;
import java.util.ArrayList;

import com.ebay.api.client.buy.model.Image;
import com.ebay.api.client.buy.model.Item;

import static com.ebay.api.ebayem.Constants.ARG_ITEM_ID;

/**
 * Manages the Item Fragment
 * Created by @Kumaresan on 3/8/2017.
 */

public class ItemFragment extends Fragment  implements Response.Listener<Item>, Response.ErrorListener, View.OnClickListener{
    private static final String TAG = "ItemFragment";

    View rootView;
    Item item;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */

    public ItemFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String itemId = getArguments().getString(ARG_ITEM_ID);

        Log.d(TAG, "onCreateView " + itemId);

        this.rootView = inflater.inflate(R.layout.fragment_item, container, false);
//        ((TextView)this.rootView.findViewById(R.id.ebay_item_text)).setText("View Item " + itemId);


        EBayEM.getInstance().getBrowseApi().getItem(itemId,this,this);


        return rootView;
    }


    private void hideProgress(View view){
        view.findViewById(R.id.progress_bar).setVisibility(View.GONE);
    }

    private void showError(View view, String text){
        ((TextView)this.rootView.findViewById(R.id.error_message)).setText(text);
        view.findViewById(R.id.error_view).setVisibility(View.VISIBLE);
    }

    private void showItemView(View view){
        view.findViewById(R.id.item_view).setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.item.getItemWebUrl()));
        startActivity(browserIntent);
    }

    @Override
    public void onResponse(Item itemResponse) {
        this.item = itemResponse;

        Log.d(TAG, "handleResponse " + this.item.toString());

        List<Image> imageList;

        if (this.item.getAdditionalImages() != null && this.item.getAdditionalImages().size() > 0) {
            imageList = this.item.getAdditionalImages();
        } else {
            imageList = new ArrayList<>();
            imageList.add(this.item.getImage());
        }

        ViewPager viewPager = (ViewPager) this.rootView.findViewById(R.id.viewPager);
        PagerAdapter adapter = new ImageSlider(this.getActivity(), imageList);
        viewPager.setAdapter(adapter);

        TextView titleTextView = (TextView) this.rootView.findViewById(R.id.title);
        TextView subTitleTextView = (TextView) this.rootView.findViewById(R.id.sub_title);
        TextView priceTextView = (TextView) this.rootView.findViewById(R.id.price);
        TextView conditionTextView = (TextView) this.rootView.findViewById(R.id.condition);
        TextView sellerTextView = (TextView) this.rootView.findViewById(R.id.seller);
        TextView itemLocationTextView = (TextView) this.rootView.findViewById(R.id.item_location);
        TextView shippingOptionTextView = (TextView) this.rootView.findViewById(R.id.shipping_option);

        titleTextView.setText(this.item.getTitle());
        subTitleTextView.setText(this.item.getSubtitle());
        priceTextView.setText("$ " + this.item.getPrice().getValue());
        conditionTextView.setText(this.item.getCondition());

        if (this.item.getSeller() != null && this.item.getSeller().getUsername() != null) {
            sellerTextView.setText(this.item.getSeller().getUsername());
        }

        itemLocationTextView.setText(this.item.getItemLocation().getCity() + "," + this.item.getItemLocation().getCountry());

        if (this.item.getShippingOptions() != null && this.item.getShippingOptions().size() > 0) {
            shippingOptionTextView.setText(this.item.getShippingOptions().get(0).getShippingServiceName());
        }

        WebView descriptionWebView = (WebView) this.rootView.findViewById(R.id.description);
        descriptionWebView.loadDataWithBaseURL("www.ebay.com", this.item.getDescription(), null, "UTF-8", null);

        Button viewItemOnEBayButton = (Button) this.rootView.findViewById(R.id.button_view_item_on_ebay);
        viewItemOnEBayButton.setOnClickListener(this);

        hideProgress(rootView);
        showItemView(rootView);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        Log.e(TAG,volleyError.getMessage());

        StringBuffer error = new StringBuffer();

        error.append(volleyError.getMessage()).append("\n");

        if(volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
            String responseBody = new String(volleyError.networkResponse.data);
            error.append(responseBody).append("\n");
            Log.e(TAG, responseBody);
        }

        showError(this.rootView,volleyError.getMessage());
        hideProgress(this.rootView);
        Toast.makeText(getActivity().getApplicationContext(), "Error! @ " + volleyError.getMessage(), Toast.LENGTH_LONG).show();
    }


}
