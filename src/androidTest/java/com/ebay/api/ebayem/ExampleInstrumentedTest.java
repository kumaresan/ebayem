package com.ebay.api.ebayem;

import android.content.Context;
import android.provider.Settings;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.core.deps.guava.base.Charsets;
import android.support.test.runner.AndroidJUnit4;
import android.util.Base64;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    public static final String TAG = "ExampleInstrumentedTest";

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.ebay.api.ebayem", appContext.getPackageName());
    }

    @Test
    public void testBasicAuthorizationBadCharacter() throws Exception{
        EBayAppConfig eBayAppConfig = new EBayAppConfig("adasdasd-7fasdf8-asdd-asdad-adsadasd","asdadadad-asd-asd-asd-asdadasdadad",true);
        AuthorizationHelper authorizationHelper = new AuthorizationHelper();
        authorizationHelper.seteBayAppConfig(eBayAppConfig);
        String basicClientAuth = authorizationHelper.getBasicClientAuth();
        URL url = new URL("https://api.ebay.com/identity/v1/oauth2/token");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.addRequestProperty("Authorization", basicClientAuth);
    }

    @Test
    public void testAuthorizationHelper() throws Exception {
        // TODO
    }

}
